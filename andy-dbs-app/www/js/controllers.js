angular.module('starter.controllers', [])

//weather controller calls functions from Weather service and initialises variables to use in view
.controller('WeatherCtrl', function($scope, Weather) {


  $scope.weather = Weather.load(); //call load function from Weather service
  $scope.remove = function(city) {
    Weather.remove(city);
  };
  
})

//city controller has parameters of scope, 'state' parameters and Weather service
.controller('CityDetailCtrl', function($scope, $stateParams, Weather) {
  $scope.city = Weather.get($stateParams.cityId);//call get function and pass in cityId obtained from navigation on clicking item in list
$scope.description = Weather.clouds($stateParams.cityId); //attempt to pass cloud description to variable description

//attempt to load a map from weather api (do we need google api?)
$scope.$on('mapInitialized', function (event,map) {
  $scope.map = map
})
})

//login controller
.controller('LoginCtrl', function($scope, $ionicModal, $timeout) {

  //initialise login variable with function that takes parameter of form submit (from view)
$scope.login = function (form) {
    if (form.$valid) {
      alert("Valid username and password fields.") //if form is valid, display this
    }

    else{
      alert("Invalid username and/or password fields.") //if form isn't valid'
    }
  };
});
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  // $scope.loginData = {};

  // // Create the login modal that we will use later
  // $ionicModal.fromTemplateUrl('templates/login.html', {
  //   scope: $scope
  // }).then(function(modal) {
  //   $scope.modal = modal;
  // });

  // // Triggered in the login modal to close it
  // $scope.closeLogin = function() {
  //   $scope.modal.hide();
  // };

  // // Open the login modal
  // $scope.login = function() {
  //   $scope.modal.show();
  // };

  // // Perform the login action when the user submits the login form
  // $scope.doLogin = function() {
  //   console.log('Doing login', $scope.loginData);

  //   // Simulate a login delay. Remove this and replace with your login
  //   // code if using a login system
  //   $timeout(function() {
  //     $scope.closeLogin();
  //   }, 1000);
  // };