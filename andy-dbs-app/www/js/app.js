
//added module packages needed for app
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngMessages', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

//changed dash controller to login controller
  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'LoginCtrl'
      }
    }
  })

//added weather tab for weather of cities list including path, html file and controller
  .state('tab.weather', {
      url: '/weather',
      views: {
        'tab-weather': {
          templateUrl: 'templates/tab-weather.html',
          controller: 'WeatherCtrl'
        }
      }
    })

    //added detail tab for city from list including path, html file and controller
    .state('tab.city-detail', {
      url: '/weather/:cityId',
      views: {
        'tab-weather': {
          templateUrl: 'templates/city-detail.html',
          controller: 'CityDetailCtrl'
        }
      }
    })

  $urlRouterProvider.otherwise('/tab/dash');

});
