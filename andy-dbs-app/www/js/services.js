angular.module('starter.services', [])

//pass on $q to utlise promise and $http to utilise data retrieval from api
.factory('Weather', function($q,$http) {

  var weather = []; //initalise empty array
  
  var coords = weather[0]

  return {
    load : function () {
      var deferred = $q.defer(); //exposes promise for asynchronous communication
      var weatherUrl = "http://api.openweathermap.org/data/2.5/group?id=7778677,2965140,2962941,2964179&units=metric&appid=e12f07c8d9437ef9fe18015f148b88b8";
      
      //get data from api and pass to function
      $http.get(weatherUrl, {}).success(function (data) {

              angular.forEach(data.list, function(city) {//for each (city) item in list array of api data, pass in
                weather.push(city);//passed in city/item is added to array
                });
                deferred.resolve(); //resolve promise/asynchronous comms
        })
        .error( function () {

              alert('There was an error'); //if error occurs from api data retrieval

        });
              return weather; //return array of cities
            },
    remove: function(city) {
      weather.splice(weather.indexOf(city), 1);
    },
    get: function(cityId) { //passed in ID from use clicking on list item
      for (var i = 0; i < weather.length; i++) {
        if (weather[i].id === parseInt(cityId)) {//match ID with IDs of all cities/items in array
          return weather[i]; //return matched city
        }
      }
      return null;
    },

//attempt at a function to retrieve inner array elements of selected weather array element
    clouds : function(cityId) {
      for (var i = 0; i < weather.length; i++) {
        if (weather[i].id === parseInt(cityId)) {
          angular.forEach(weather[i].weather, function(properties) {
            if (properties.id == 803)
            {
              return properties;
            }
          })

          }
        }
      }
    }
  });
